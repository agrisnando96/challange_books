const books = require('../models/book.js')

module.exports = (app)=>{
app.post("/books", async (request, response) => {
      try {
        var book = new books.model(request.body);
        var result = await book.save();
        response.send(result);
      } catch (error) {
        response.status(500).send(error);
      }
    });

    //view all books
    app.get("/books", async (request, response) => {
          try {
            var book = await books.model.find().exec();
            response.send(book);
          } catch (error) {
            response.status(500).send(error);
          }
        });


    //view id
    app.get("/books/:id", async (request, response) => {
          try {
            var book = await books.model.findById(request.params.id).exec();
            response.send(book);
          } catch (error) {
            response.status(500).send(error);
          }
        });

        //update
        app.put("/books/:id", async (request, response) => {
              try {
                var book = await books.model.findById(request.params.id).exec();
                book.set(request.body);
                var result = await book.save();
                response.send(result);
              } catch (error) {
                response.status(500).send(error);
              }
            });
            
            //delete
            app.delete("/books/:id", async (request, response) => {
                  try {
                    var result = await books.model.deleteOne({ _id: request.params.id }).exec();
                    response.send(result);
                  } catch (error) {
                    response.status(500).send(error);
                  }
                });

               
            }
        