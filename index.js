const Express = require("express");
const BodyParser = require("body-parser");

const app = Express();
const PORT = 3000;
const DOMAIN = 'localhost';

app.use(Express.json());
app.use(BodyParser.urlencoded({ extended: true }));

require('./route/route.js')(app);

app.listen(PORT, DOMAIN, () => {
    console.log(`Server is listening on http://${DOMAIN}:${PORT}`);
});