const Mongoose = require("mongoose")

Mongoose.connect("mongodb://localhost:27017/db_buku",{
    useNewUrlParser:true,
    useUnifiedTopology:true
});


const BookModel = Mongoose.model("book", {
      title: {
      type : String,
      require: [true, "title require"]
    },
      author: {
      type : String,
      require: [true, "author require"]
    },
      published_date : Date,

      pages: {
      type : Number,
      require: [true, "pages require"]
    },
      language : {
      type : String,
      require: [true, "leanguage require"]
      },
      publisher_id : {
        type : String,
        require: [true, "publisher_id require"]
      },
    })

exports.model = BookModel;

